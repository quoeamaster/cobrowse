// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// cobrowse - headless browser
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"testing"
	"time"

	"gitlab.com/quoeamaster/cobrowse/internal"
	"gitlab.com/quoeamaster/cobrowse/pkg/util"
)

// Test_dateINISO8601_00 tests the formatting of a [time/Time] object to ISO8601.
func Test_dateInISO8601_00(t *testing.T) {
	line, start := internal.UTestHeader("format_test", "Test_dateInISO8601_00", true)
	t.Log(line)

	// create some dates
	tz, _ := time.LoadLocation("Asia/Shanghai")
	type _test struct {
		src  time.Time
		dest []string
	}
	_tests := []_test{
		{src: time.Date(2010, 11, 25, 0, 0, 0, 0, time.UTC), dest: []string{"2010-11-25T00:00:00Z0000", "2010-11-25T00:00:00Z"}},
		{src: time.Date(2090, 12, 31, 15, 2, 45, 0, tz), dest: []string{"2090-12-31T15:02:45+0800"}},
	}
	// run the table test(s)
	for _, _case := range _tests {
		v := util.DateInISO8601(_case.src)
		formats := _case.dest
		matched := false
		for _, format := range formats {
			if format == v {
				matched = true
				break
			}
		}
		if !matched {
			t.Fatalf("failed to match iso8601 for %v to any of the %v, returned value: %v\n", _case.src, _case.dest, v)
		}
	}
	// ends
	line, _ = internal.UTestTrailer("format_test", "Test_dateInISO8601_00", true, start)
	t.Log(line)
}
