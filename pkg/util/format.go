// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// cobrowse - headless browser
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package util

import (
	"time"
)

const (
	// FORMAT_ISO8601 represents the iso8601 date time format. Please check that iso8601 does NOT match with RFC3339 completely.
	FORMAT_ISO8601 string = "2006-01-02T15:04:05Z0700"
)

// DateInISO8601 returns the a string presentation of the provided date argument.
func DateInISO8601(date time.Time) (s string) {
	s = date.Format(FORMAT_ISO8601)
	return
}
