// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// cobrowse - headless browser
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package internal

import (
	"bytes"
	"fmt"
	"time"
)

const (
	sectionSeparator string = "-------------------"
)

func UTestHeader(testName, funcName string, needTimestamp bool) (s string, tstamp time.Time) {
	if true == needTimestamp {
		tstamp = time.Now()
		s = fmt.Sprintf("%v\n*  %v.%v (started at: %v)  *\n", sectionSeparator, testName, funcName, tstamp.Format("2006-01-02T15:04:05Z0700"))
	} else {
		s = fmt.Sprintf("%v\n*  %v.%v (starts)  *\n", sectionSeparator, testName, funcName)
	}
	return
}

func UTestTrailer(testName, funcName string, needTimestamp bool, startTStamp time.Time) (s string, diffInNano int64) {
	b := bytes.NewBuffer(nil)
	now := time.Now()
	var diff time.Duration

	// if startTStamp is provided... calculate the difference in ms between NOW and the provided argument
	if false == startTStamp.IsZero() {
		diff = now.Sub(startTStamp)
		diffInNano = diff.Nanoseconds()
		b.WriteString(fmt.Sprintf("**  duration of test in MS: %v  **\n", diffInNano))
	}

	if true == needTimestamp {
		b.WriteString(fmt.Sprintf("%v\n*  %v.%v (ended at: %v)  *\n", sectionSeparator, testName, funcName, now.Format("2006-01-02T15:04:05Z0700")))
	} else {
		b.WriteString(fmt.Sprintf("%v\n*  %v.%v (ends)  *\n", sectionSeparator, testName, funcName))
	}
	s = b.String()
	return
}
